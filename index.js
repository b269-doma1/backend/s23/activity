

let trainer = { talk:function(){
	console.log("Pikachu i choose you!")
}};

trainer.name = 'Ash Ketchum';

trainer.age = 10;

trainer['pokemon'] = ['Pikachu', 'Charmander', 'Bulbasaur', 'Squirtle']


trainer.friends = {
	hoen: ['May', 'Max'],
	kanto: ['Brock', 'Misty']
}


console.log(trainer);

console.log("Result at dot notation:")
console.log(trainer.name);
console.log("Result of square bracket notation:")
console.log(trainer['pokemon']);
trainer.talk();



function Pokemon(name, level) {

    // Properties
    this.name = name;
    this.level = level;
    this.health = 2 * level ;
    this.attack = level;

    //Methods
    this.tackle = function(target) {
        console.log(this.name + ' tackled ' + target.name);
        target.health = target.health - this.attack
        console.log(target.name + " health is now reduced to " + target.health );

       	if (target.health <=0){
       		target.faint();
       	}
    
        };
    this.faint = function(){
    	
        console.log(this.name + ' fainted.');
    }

}


let pikachu = new Pokemon("Pikachu", 12)	
console.log(pikachu);
let geodude = new Pokemon('Geodude', 8);
console.log(geodude);
let mewtwo = new Pokemon("Mewtwo", 100);
console.log(mewtwo);

geodude.tackle(pikachu);
mewtwo.tackle(geodude);
console.log(geodude);















